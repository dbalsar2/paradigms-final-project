import cherrypy
import re, json
from disaster_library import _disaster_database

class ResetController(object):

        def __init__(self, cdb=None):
                if cdb is None:
                        self.cdb = _disaster_database()
                else:
                        self.cdb = cdb


        def PUT_INDEX(self):
                output = {'result':'success'}

                data = json.loads(cherrypy.request.body.read().decode())

                self.cdb.__init__()
                self.cdb.load_disasters('./disasters.json')

                return json.dumps(output)

        def PUT_KEY(self, country_id):
                output = {'result':'success'}
                cid = int(country_id)

                try:
                        data = json.loads(cherrypy.request.body.read().decode())

                        cdbtmp = _disaster_database()
                        cdbtmp.load_disasters('./disasters.json')

                        country  = cdbtmp.get_country(cid)

                        self.cdb.set_country(cid, country)

                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)
