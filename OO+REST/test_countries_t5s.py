import unittest
import requests
import json


class TestT5s(unittest.TestCase):

        SITE_URL = 'http://student05.cse.nd.edu:51028'
        FREQUENT_URL = SITE_URL + '/countries/frequent/'
        DEATHS_URL = SITE_URL + '/countries/deaths/'
        PERSONS_URL = SITE_URL + '/countries/persons/'

        def is_json(self, resp):
                try:
                        json.loads(resp)
                        return True
                except ValueError:
                        return False


        def test_frequency(self):
                r = requests.get(self.FREQUENT_URL)
                self.assertTrue(self.is_json(r.content.decode('utf-8')))
                resp = json.loads(r.content.decode())
                print(resp)
                self.assertEqual(resp['result'], 'success')

        def test_deaths(self):
                r = requests.get(self.DEATHS_URL)
                self.assertTrue(self.is_json(r.content.decode('utf-8')))
                resp = json.loads(r.content.decode())
                print(resp)
                self.assertEqual(resp['result'], 'success')

        def test_persons(self):
                r = requests.get(self.PERSONS_URL)
                self.assertTrue(self.is_json)
                resp = json.loads(r.content.decode())
                print(resp)
                self.assertEqual(resp['result'], 'success')

if __name__ == "__main__":
        unittest.main()
