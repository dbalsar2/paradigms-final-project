import json


class _disaster_database:

        def __init__(self):
                self.terms = []
                self.country_names = dict()
                self.range_one = dict()
                self.range_two = dict()
                self.range_three = dict()
                self.range_four = dict()
                self.range_five = dict()
                self.range_six = dict()
                self.range_seven = dict()
                self.range_eight = dict()
                self.range_nine = dict()

        def load_disasters(self, path):
                with open(path) as f:
                        data = json.load(f)
                for item in data:
                        self.terms.append(item)
                        cid = int(item[0])
                        cname = item[1]
                        range_one = item[2]
                        range_two = item[3]
                        range_three = item[4]
                        range_four = item[5]
                        range_five = item[6]
                        range_six = item[7]
                        range_seven = item[8]
                        range_eight = item[9]
                        range_nine = item[10]
                        self.country_names[cid] = cname
                        self.range_one[cid] = range_one
                        self.range_two[cid] = range_two
                        self.range_three[cid] = range_three
                        self.range_four[cid] = range_four
                        self.range_five[cid] = range_five
                        self.range_six[cid] = range_six
                        self.range_seven[cid] = range_seven
                        self.range_eight[cid] = range_eight
                        self.range_nine[cid] = range_nine

        def get_countries(self):
                return self.country_names.keys()

        def get_country(self, cid):
                print("entered get by id")
                try:
                        cname = self.country_names[cid]
                        range_one = self.range_one[cid]
                        range_two = self.range_two[cid]
                        range_three = self.range_three[cid]
                        range_four = self.range_four[cid]
                        range_five = self.range_five[cid]
                        range_six = self.range_six[cid]
                        range_seven = self.range_seven[cid]
                        range_eight = self.range_eight[cid]
                        range_nine = self.range_nine[cid]
                        country = list((cname, range_one, range_two, range_three, range_four, range_five, range_six, range_seven, range_eight, range_nine))
                except Exception as ex:
                        country = None
                return country

        def get_country_by_name(self, cname):
                for item in self.country_names.items():
                        if cname == item[1]:
                                range_one = self.range_one[item[0]]
                                range_two = self.range_two[item[0]]
                                range_three = self.range_three[item[0]]
                                range_four = self.range_four[item[0]]
                                range_five = self.range_five[item[0]]
                                range_six = self.range_six[item[0]]
                                range_seven = self.range_seven[item[0]]
                                range_eight = self.range_eight[item[0]]
                                range_nine = self.range_nine[item[0]]
                                country = list((cname, range_one, range_two, range_three, range_four, range_five, range_six, range_seven, range_eight, range_nine))
                                return country


        def set_country(self, cid, country):
                self.country_names[cid] = country[0]
                self.range_one[cid] = country[1]
                self.range_two[cid] = country[2]
                self.range_three[cid] = country[3]
                self.range_four[cid] = country[4]
                self.range_five[cid] = country[5]
                self.range_six[cid] = country[6]
                self.range_seven[cid] = country[7]
                self.range_eight[cid] = country[8]
                self.range_nine[cid] = country[9]

                if cid not in self.country_names.keys():
                        self.country_names[cid] = dict()


        def delete_country(self, cid):
                del(self.country_names[cid])
                del(self.range_one[cid])
                del(self.range_two[cid])
                del(self.range_three[cid])
                del(self.range_four[cid])
                del(self.range_five[cid])
                del(self.range_six[cid])
                del(self.range_seven[cid])
                del(self.range_eight[cid])
                del(self.range_nine[cid])

        def get_most_frequently_affected(self):
                max_1 = 0
                max_2 = 0
                max_3 = 0
                max_4 = 0
                max_5 = 0
                max_1key = 0
                max_2key = 0
                max_3key = 0
                max_4key = 0
                max_5key = 0
                countryList = []
                cid = 0
                countries = list(self.get_countries())
                for key in countries:
                        country = self.get_country(key)
                        try:
                                value = int(country[3])
                        except:
                                continue
                        if(value >= max_1):
                                max_5 = max_4
                                max_5key = max_4key
                                max_4 = max_3
                                max_4key = max_3key
                                max_3 = max_2
                                max_3key = max_2key
                                max_2 = max_1
                                max_2key = max_1key
                                max_1 = value
                                max_1key = key
                        elif(value >= max_2):
                                max_5 = max_4
                                max_5key = max_4key
                                max_4 = max_3
                                max_4key = max_3key
                                max_3 = max_2
                                max_3key = max_2key
                                max_2 = value
                                max_2key = key
                        elif(value >= max_3):
                                max_5 = max_4
                                max_5key = max_4key
                                max_4 = max_3
                                max_4key = max_3key
                                max_3 = value
                                max_3key = key
                        elif(value >= max_4):
                                max_5 = max_4
                                max_5key = max_4key
                                max_4 = value
                                max_4key = key
                        elif(value >= max_5):
                                max_5 = value
                                max_5key = key


                countryList.append(self.get_country(max_1key))
                countryList.append(self.get_country(max_2key))
                countryList.append(self.get_country(max_3key))
                countryList.append(self.get_country(max_4key))
                countryList.append(self.get_country(max_5key))

                return countryList

        def get_most_deaths(self):
                max_1 = 0
                max_2 = 0
                max_3 = 0
                max_4 = 0
                max_5 = 0
                max_1key = 0
                max_2key = 0
                max_3key = 0
                max_4key = 0
                max_5key = 0
                countryList = []
                cid = 0
                countries = list(self.get_countries())
                for key in countries:
                        country = self.get_country(key)
                        try:
                                value = int(country[6])
                        except:
                                continue
                        if(value >= max_1):
                                max_5 = max_4
                                max_5key = max_4key
                                max_4 = max_3
                                max_4key = max_3key
                                max_3 = max_2
                                max_3key = max_2key
                                max_2 = max_1
                                max_2key = max_1key
                                max_1 = value
                                max_1key = key
                        elif(value >= max_2):
                                max_5 = max_4
                                max_5key = max_4key
                                max_4 = max_3
                                max_4key = max_3key
                                max_3 = max_2
                                max_3key = max_2key
                                max_2 = value
                                max_2key = key
                        elif(value >= max_3):
                                max_5 = max_4
                                max_5key = max_4key
                                max_4 = max_3
                                max_4key = max_3key
                                max_3 = value
                                max_3key = key
                        elif(value >= max_4):
                                max_5 = max_4
                                max_5key = max_4key
                                max_4 = value
                                max_4key = key
                        elif(value >= max_5):
                                max_5 = value
                                max_5key = key

                countryList.append(self.get_country(max_1key))
                countryList.append(self.get_country(max_2key))
                countryList.append(self.get_country(max_3key))
                countryList.append(self.get_country(max_4key))
                countryList.append(self.get_country(max_5key))

                return countryList

        def get_most_affected(self):
                max_1 = 0
                max_2 = 0
                max_3 = 0
                max_4 = 0
                max_5 = 0
                max_1key = 0
                max_2key = 0
                max_3key = 0
                max_4key = 0
                max_5key = 0
                countryList = []
                cid = 0
                countries = list(self.get_countries())
                for key in countries:
                        country = self.get_country(key)
                        try:
                                value = int(country[9])
                        except:
                                continue
                        if(value >= max_1):
                                max_5 = max_4
                                max_5key = max_4key
                                max_4 = max_3
                                max_4key = max_3key
                                max_3 = max_2
                                max_3key = max_2key
                                max_2 = max_1
                                max_2key = max_1key
                                max_1 = value
                                max_1key = key
                        elif(value >= max_2):
                                max_5 = max_4
                                max_5key = max_4key
                                max_4 = max_3
                                max_4key = max_3key
                                max_3 = max_2
                                max_3key = max_2key
                                max_2 = value
                                max_2key = key
                        elif(value >= max_3):
                                max_5 = max_4
                                max_5key = max_4key
                                max_4 = max_3
                                max_4key = max_3key
                                max_3 = value
                                max_3key = key
                        elif(value >= max_4):
                                max_5 = max_4
                                max_5key = max_4key
                                max_4 = value
                                max_4key = key
                        elif(value >= max_5):
                                max_5 = value
                                max_5key = key


                countryList.append(self.get_country(max_1key))
                countryList.append(self.get_country(max_2key))
                countryList.append(self.get_country(max_3key))
                countryList.append(self.get_country(max_4key))
                countryList.append(self.get_country(max_5key))

                return countryList




if __name__ == "__main__":
        cdb = _disaster_database()

        cdb.load_disasters('./disasters.json')
