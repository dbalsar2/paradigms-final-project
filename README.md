# Paradigms Final Project


OO + API:

| Command | Resource |  Input Example | Output Example   

| GET     | /countries/:cid |             | {"name" : "Afghanistan", "range_1": "265", "range_2" : "34", "range_3": "12", "range_5": "13" ...}

| PUT    | /countries/:cid |  {"name" : "Afghanistan", "range_1": "265", "range_2" : "34", "range_3": "12", "range_5": "13" ...} | {"result": "success"}

| DELETE | /countries/:cid |          |   {"result": "success"}

| GET    | /countries/ |              |  [{"name" : "Afghanistan", "range_1": "265", "range_2" : "34", "range_3": "12", "range_5": "13" ...} ... (the rest of the country data in the JSON) ]

| POST   | /countries/ |  {"name" : "Afghanistan", "range_1": "265", "range_2" : "34", "range_3": "12", "range_5": "13" ...} | {"result": "success"}

| DELETE | /countries/ |              |  {"result": "success"}



Complexity:

The scale of our project was apporpriate for the project parameters. The complexity level was high, as we added many of our own backend functions that are distinct from what we learned in class. For example, we added functions to get analytics on the top 5 countries most impacted by natural disasters in different ways, requiring us to produce original algorithms that would extract useful data from lists on the backend.  In doing so, we added our own original thought processes and coding techniques to other insights we gained from the course such as client to server computing. Our project also addresses a niche yet expansive problem (meteorological disasters) that is often not talked about as a source of suffering for some countries. We even go as far as to add resources if a user wants to donate to organizations such as the Red Cross which work to aid countries affected by natural disasters, facilitating progress on this high scale problem. The project combines concepts discussed in class such as APIs, REST servers, and frontend HTML and JavaScript programming with a creative approach to data structures and algorithms as well as an intent to solve a macro-level problem from the real world, demonstrating sufficient coplexity and scale.