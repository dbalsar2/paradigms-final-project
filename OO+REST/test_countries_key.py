import unittest
import json
import requests

class TestCountries(unittest.TestCase):
        SITE_URL = 'http://student05.cse.nd.edu:51028'
        COUNTRIES_URL = SITE_URL + '/countries/'
        RESET_URL = SITE_URL + '/reset/'

        def reset_data(self):
                c = {}
                r = requests.put(self.RESET_URL, data = json.dumps(c))

        def is_json(self, resp):
                try:
                        json.loads(resp)
                        return True
                except ValueError:
                        return False

        def test_countries_get_key(self):
                self.reset_data()
                country_id = 4
                r = requests.get(self.COUNTRIES_URL + str(country_id))
                self.assertTrue(self.is_json(r.content.decode('utf-8')))
                resp = json.loads(r.content.decode('utf-8'))
                self.assertEqual(resp['Country'], 'Afghanistan')
                self.assertEqual(resp['range_one'], '3')

        def test_countries_put_key(self):
                self.reset_data()
                country_id = 8
                r = requests.get(self.COUNTRIES_URL + str(country_id))
                self.assertTrue(self.is_json(r.content.decode('utf-8')))
                resp = json.loads(r.content.decode('utf-8'))
                self.assertEqual(resp['Country'], 'Albania')
                self.assertEqual(resp['range_one'], '...')

                c = {}
                c['country'] = 'Hypothetical Country'
                c['range_one'] = '1'
                c['range_two'] = '1'
                c['range_three'] = '1'
                c['range_four'] = '1'
                c['range_five'] = '1'
                c['range_six'] = '1'
                c['range_seven'] = '1'
                c['range_eight'] = '1'
                c['range_nine'] = '1'
                r = requests.put(self.COUNTRIES_URL + str(country_id), data = json.dumps(c))
                self.assertTrue(self.is_json(r.content.decode('utf-8')))
                resp = json.loads(r.content.decode('utf-8'))
                self.assertEqual(resp['result'], 'success')

                r = requests.get(self.COUNTRIES_URL + str(country_id))
                self.assertTrue(self.is_json(r.content.decode('utf-8')))
                resp = json.loads(r.content.decode('utf-8'))
                self.assertEqual(resp['Country'], c['country'])
                self.assertEqual(resp['range_one'], c['range_one'])

        def test_countries_delete_key(self):
                self.reset_data()
                country_id = 8

                c = {}
                r = requests.delete(self.COUNTRIES_URL + str(country_id), data = json.dumps(c))
                self.assertTrue(self.is_json(r.content.decode('utf-8')))
                resp = json.loads(r.content.decode('utf-8'))
                self.assertEqual(resp['result'], 'success')

                r = requests.get(self.COUNTRIES_URL + str(country_id))
                self.assertTrue(self.is_json(r.content.decode('utf-8')))
                resp = json.loads(r.content.decode('utf-8'))
                self.assertEqual(resp['result'], 'error')


if __name__ == "__main__":
        unittest.main()
