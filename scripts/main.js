console.log('page load - entered main.js for js-other api');

window.onload = fillTables;
var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;


function fillTables(){
    console.log('entered fillTables!');

    var xhr = new XMLHttpRequest();
    var url = "http://student05.cse.nd.edu:51028/countries/frequent/";
    xhr.open("GET", url, true);

    xhr.onload = function(e) {
        console.log(xhr.responseText);

        fillTable1(xhr.responseText);
    }
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    xhr.send(null);

    var xhr_2 = new XMLHttpRequest();
    var url_2 = "http://student05.cse.nd.edu:51028/countries/deaths/";
    xhr_2.open("GET", url_2, true);

    xhr_2.onload = function(e) {
        console.log(xhr_2.responseText);

        fillTable2(xhr_2.responseText);
    }
    xhr_2.onerror = function(e){
        console.error(xhr_2.statusText);
    }

    xhr_2.send(null);

    var xhr_3 = new XMLHttpRequest();
    var url_3 = "http://student05.cse.nd.edu:51028/countries/persons/";
    xhr_3.open("GET", url_3, true);

    xhr_3.onload = function(e){
        console.log(xhr_3.responseText);

        fillTable3(xhr_3.responseText);
    }
    xhr_3.onerror = function(e){
        console.error(xhr_3.statusText);
    }

    xhr_3.send(null);
    
}


function fillTable1(response_text){
    var response_json = JSON.parse(response_text);

    var row1 = document.getElementById("occur-line1");
    var row2 = document.getElementById("occur-line2");
    var row3 = document.getElementById("occur-line3");
    var row4 = document.getElementById("occur-line4");
    var row5 = document.getElementById("occur-line5");

    row1.innerHTML = "1. " + response_json['target'][0][0] + " - " + response_json['target'][0][3];
    row2.innerHTML = "2. " + response_json['target'][1][0] + " - " + response_json['target'][1][3];
    row3.innerHTML = "3. " + response_json['target'][2][0] + " - " + response_json['target'][2][3];
    row4.innerHTML = "4. " + response_json['target'][3][0] + " - " + response_json['target'][3][3];
    row5.innerHTML = "5. " + response_json['target'][4][0] + " - " + response_json['target'][4][3];

}

function fillTable2(response_text){
    var response_json = JSON.parse(response_text);

    var row1 = document.getElementById("death-line1");
    var row2 = document.getElementById("death-line2");
    var row3 = document.getElementById("death-line3");
    var row4 = document.getElementById("death-line4");
    var row5 = document.getElementById("death-line5");

    row1.innerHTML = "1. " + response_json['target'][0][0] + " - " + response_json['target'][0][6];
    row2.innerHTML = "2. " + response_json['target'][1][0] + " - " + response_json['target'][1][6];
    row3.innerHTML = "3. " + response_json['target'][2][0] + " - " + response_json['target'][2][6];
    row4.innerHTML = "4. " + response_json['target'][3][0] + " - " + response_json['target'][3][6];
    row5.innerHTML = "5. " + response_json['target'][4][0] + " - " + response_json['target'][4][6];
}


function fillTable3(response_text){
    var response_json = JSON.parse(response_text);

    var row1 = document.getElementById("affected-line1");
    var row2= document.getElementById("affected-line2");
    var row3 = document.getElementById("affected-line3");
    var row4 = document.getElementById("affected-line4");
    var row5 = document.getElementById("affected-line5");

    row1.innerHTML = "1. " + response_json['target'][0][0] + " - " + response_json['target'][0][9];
    row2.innerText = "2. " + response_json['target'][1][0] + " - " + response_json['target'][1][9];
    row3.innerHTML = "3. " + response_json['target'][2][0] + " - " + response_json['target'][2][9];
    row4.innerHTML = "4. " + response_json['target'][3][0] + " - " + response_json['target'][3][9];
    row5.innerHTML = "5. " + response_json['target'][4][0] + " - " + response_json['target'][4][9];
}

function getFormInfo(){
    console.log('entered getFormInfo!');
    // call displayinfo
    var country = document.getElementById("country-text").value;
    console.log('Number you entered is ' + name);
    makeNetworkCallToCountryApi(country);

} // end of get form info

function makeNetworkCallToCountryApi(country){
    console.log('entered make nw call' + country);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "http://student05.cse.nd.edu:51028/countryName/" + country;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateResponseWithCountry(country, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateResponseWithCountry(country, response_text){
    var response_json = JSON.parse(response_text);
    // update a label
    var label1 = document.getElementById("response-line1");

    if(response_json['country'] == null){
        label1.innerHTML = 'Apologies, we could not find that country.'
    } else{
        label1.innerHTML =  'In ' + country + ', there were ' + response_json['total_occurences'] + ' occurrences of natural disasters, ' + response_json['total_deaths'] + ' deaths caused by disasters, and ' + response_json['total_affected'] + ' people affected by natural disasters from 1990 to 2019.';
    }
} // end of updateAgeWithResponse

/*
function makeNetworkCallToNumbers(probability){
    console.log('entered make nw call' + probability);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://api.fda.gov/food/enforcement.json?limit=" + probability;
    xhr.open("GET", url, true) // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateTriviaWithResponse(probability, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateTriviaWithResponse(probability, response_text){
    // update a label
    var label2 = document.getElementById("response-line2");
    label2.innerHTML = response_text;

    // dynamically adding label
    label_item = document.createElement("label"); // "label" is a classname
    label_item.setAttribute("id", "dynamic-label" ); // setAttribute(property_name, value) so here id is property name of button object

    var item_text = document.createTextNode(response_text); // creating new text
    label_item.appendChild(item_text); // adding something to button with appendChild()

    // option 1: directly add to document
    // adding label to document
    //document.body.appendChild(label_item);

    // option 2:
    // adding label as sibling to paragraphs
    var response_div = document.getElementById("response-div");
    response_div.appendChild(label_item);

} // end of updateTriviaWithResponse
*/