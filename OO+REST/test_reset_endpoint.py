import unittest
import requests
import json

class TestReset(unittest.TestCase):

        SITE_URL = 'http://student05.cse.nd.edu:51028'
        COUNTRIES_URL = SITE_URL + '/countries/'
        RESET_URL = SITE_URL + '/reset/'

        def is_json(self, resp):
                try:
                        json.loads(resp)
                        return True
                except ValueError:
                        return False


        def test_put_reset_index(self):
                c = {}
                r = requests.put(self.RESET_URL, json.dumps(c))
                self.assertTrue(self.is_json(r.content.decode('utf-8')))
                resp = json.loads(r.content.decode())
                self.assertEqual(resp['result'], 'success')

                r2 = requests.get(self.COUNTRIES_URL + "4")
                self.assertTrue(self.is_json(r2.content.decode('utf-8')))
                resp2 = json.loads(r2.content.decode())

                r3 = requests.get(self.COUNTRIES_URL + "8")
                self.assertTrue(self.is_json(r3.content.decode('utf-8')))
                resp3 = json.loads(r3.content.decode())

                self.assertEqual(resp2['Country'], 'Afghanistan')
                self.assertEqual(resp2['range_one'], '3')
                self.assertEqual(resp3['Country'], 'Albania')
                self.assertEqual(resp3['range_one'], '...')

        def test_put_reset_key(self):
                c = {}
                r = requests.put(self.RESET_URL + "4", json.dumps(c))
                self.assertTrue(self.is_json(r.content.decode('utf-8')))
                resp = json.loads(r.content.decode())
                self.assertEqual(resp['result'], 'success')

                r2 = requests.get(self.COUNTRIES_URL + "4")
                self.assertTrue(self.is_json(r2.content.decode('utf-8')))
                resp2 = json.loads(r2.content.decode())

                self.assertEqual(resp2['Country'], 'Afghanistan')
                self.assertEqual(resp2['range_one'], '3')


if __name__ == "__main__":
        unittest.main()
