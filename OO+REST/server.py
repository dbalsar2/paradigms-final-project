import cherrypy
from countriesController import CountryController
from resetController import ResetController
from disaster_library import _disaster_database

class optionsController:
        def OPTIONS(self, *args, **kwargs):
                return ""

def CORS():
        cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
        cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
        cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

def start_service():
        dispatcher = cherrypy.dispatch.RoutesDispatcher()

        cdb = _disaster_database()

        countryController = CountryController(cdb=cdb)
        resetController = ResetController(cdb=cdb)

        dispatcher.connect('country_get', '/countries/:country_id', controller=countryController, action = 'GET_KEY', conditions=dict(method=['GET']))
        dispatcher.connect('country_put', '/countries/:country_id', controller=countryController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
        dispatcher.connect('country_delete', '/countries/:country_id', controller=countryController, action = 'DELETE_KEY', conditions=dict(method=['DELETE']))
        dispatcher.connect('country_index_get', '/countries/', controller=countryController, action = 'GET_INDEX', conditions=dict(method=['GET']))
        dispatcher.connect('country_index_post', '/countries/', controller=countryController, action = 'POST_INDEX', conditions=dict(method=['POST']))
        dispatcher.connect('country_index_delete', '/countries/', controller=countryController, action = 'DELETE_INDEX', conditions=dict(method=['DELETE']))

        dispatcher.connect('reset_put', '/reset/:country_id', controller=resetController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
        dispatcher.connect('reset_index_put', '/reset/', controller=resetController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))

        dispatcher.connect('get_most_frequently_affected', '/countries/frequent/', controller=countryController, action = 'GET_MOST_FREQUENT', conditions=dict(method=['GET']))
        dispatcher.connect('get_most_deaths', '/countries/deaths/', controller=countryController, action = 'GET_MOST_DEATHS', conditions=dict(method=['GET']))
        dispatcher.connect('get_most_affected', '/countries/persons/', controller=countryController, action = 'GET_MOST_AFFECTED', conditions=dict(method=['GET']))

        dispatcher.connect('get_country_by_name', '/countryName/:cname', controller=countryController, action = 'GET_BY_NAME', conditions=dict(method=['GET']))

        conf = {
                'global': {
                        'server.thread_pool': 5,
                        'server.socket_host': 'student05.cse.nd.edu',
                        'server.socket_port': 51028,
                        },
                '/': {
                        'request.dispatch': dispatcher,
                }
        }

        cherrypy.config.update(conf)
        app = cherrypy.tree.mount(None, config=conf)
        cherrypy.quickstart(app)


if __name__ == '__main__':
        cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
        start_service()
