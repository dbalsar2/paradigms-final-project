import unittest
import requests
import json

class TestCountriesIndex(unittest.TestCase):

        SITE_URL = 'http://student05.cse.nd.edu:51028'
        COUNTRIES_URL = SITE_URL + '/countries/'
        RESET_URL = SITE_URL + '/reset/'

        def reset_data(self):
                c = {}
                r = requests.put(self.RESET_URL, json.dumps(c))

        def is_json(self, resp):
                try:
                        json.loads(resp)
                        return True
                except ValueError:
                        return False

        def test_countries_index_get(self):
                self.reset_data()
                r = requests.get(self.COUNTRIES_URL)
                self.assertTrue(self.is_json(r.content.decode()))
                resp = json.loads(r.content.decode())

                testcountry = {}
                countries = resp['countries']
                for country in countries:
                        if country['id'] == 8:
                                testcountry = country

                self.assertEqual(testcountry['country'], 'Albania')
                self.assertEqual(testcountry['range_one'], '...')

        def test_countries_index_post(self):
                self.reset_data()

                c = {}
                c['country'] = 'Hypothetical Country'
                c['range_one'] = '1'
                c['range_two'] = '2'
                c['range_three'] = '3'
                c['range_four'] = '4'
                c['range_five'] = '5'
                c['range_six'] = '6'
                c['range_seven'] = '7'
                c['range_eight'] = '8'
                c['range_nine'] = '9'
                r = requests.post(self.COUNTRIES_URL, data = json.dumps(c))
                self.assertTrue(self.is_json(r.content.decode()))
                resp = json.loads(r.content.decode())
                self.assertEqual(resp['result'], 'success')
                self.assertEqual(resp['id'], 717)

                r = requests.get(self.COUNTRIES_URL + str(resp['id']))
                self.assertTrue(self.is_json(r.content.decode()))
                resp = json.loads(r.content.decode())
                self.assertEqual(resp['Country'], c['country'])
                self.assertEqual(resp['range_one'], c['range_one'])

        def test_countries_index_delete(self):
                self.reset_data()

                c = {}
                r = requests.delete(self.COUNTRIES_URL, data = json.dumps(c))
                self.assertTrue(self.is_json(r.content.decode()))
                resp = json.loads(r.content.decode())
                self.assertEqual(resp['result'], 'success')

                r = requests.get(self.COUNTRIES_URL)
                self.assertTrue(self.is_json(r.content.decode()))
                resp = json.loads(r.content.decode())
                countries = resp['countries']
                self.assertFalse(countries)

if __name__ == "__main__":
        unittest.main()
