import cherrypy
import re, json
from disaster_library import _disaster_database

class CountryController(object):

        def __init__(self, cdb=None):
                if cdb is None:
                        self.cdb = _disaster_database()
                else:
                        self.cdb = cdb

                self.cdb.load_disasters('./disasters.json')

        def GET_KEY(self, country_id):
                output = {'result': 'success'}
                cid = int(country_id)
                try:
                        country = self.cdb.get_country(cid)
                        if country is not None:
                                output['id'] = cid
                                output['Country'] = country[0]
                                output['range_one'] = country[1]
                                output['range_two'] = country[2]
                                output['range_three'] = country[3]
                                output['range_four'] = country[4]
                                output['range_five'] = country[5]
                                output['range_six'] = country[6]
                                output['range_seven'] = country[7]
                                output['range_eight'] = country[8]
                                output['range_nine'] = country[9]
                        else:
                                output['result'] = 'error'
                                output['message'] = 'country not found'
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)

        def PUT_KEY(self, country_id):
                output = {'result': 'success'}
                cid = int(country_id)

                data = json.loads(cherrypy.request.body.read().decode('utf-8'))

                country = list()
                country.append(data['country'])
                country.append(data['range_one'])
                country.append(data['range_two'])
                country.append(data['range_three'])
                country.append(data['range_four'])
                country.append(data['range_five'])
                country.append(data['range_six'])
                country.append(data['range_seven'])
                country.append(data['range_eight'])
                country.append(data['range_nine'])

                self.cdb.set_country(cid, country)

                return json.dumps(output)

        def DELETE_KEY(self, country_id):
                output= {'result' : 'success'}
                try:
                        self.cdb.delete_country(int(country_id))
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)

        def GET_INDEX(self):
                output = {'result':'success'}
                output['countries'] = []

                try:
                        for cid in self.cdb.get_countries():
                                country = self.cdb.get_country(cid)
                                dcountry = {'id': cid, 'country':country[0], 'range_one':country[1],
                                                        'range_two':country[2], 'range_three':country[3], 'range_four':country[4],
                                                        'range_five':country[5], 'range_six':country[6], 'range_seven':country[7],
                                                        'range_eight':country[8], 'range_nine':country[9] }
                                output['countries'].append(dcountry)
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)

        def POST_INDEX(self):
                output = {'result': 'success'}
                data = json.loads(cherrypy.request.body.read().decode('utf-8'))

                country = list()
                country.append(data['country'])
                country.append(data['range_one'])
                country.append(data['range_two'])
                country.append(data['range_three'])
                country.append(data['range_four'])
                country.append(data['range_five'])
                country.append(data['range_six'])
                country.append(data['range_seven'])
                country.append(data['range_eight'])
                country.append(data['range_nine'])

                try:
                        country_id = 0
                        for cid in self.cdb.get_countries():
                                country_id = cid
                        country_id = country_id + 1
                        self.cdb.set_country(country_id, country)
                        output['id'] = country_id
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)

        def DELETE_INDEX(self):
                output = { 'result': 'success' }

                try:
                        countries = list(self.cdb.get_countries())
                        for country in countries:
                                self.cdb.delete_country(country)
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)


                return json.dumps(output)

        def GET_MOST_FREQUENT(self):
                output = { 'result': 'success' }

                try:
                        print('entering function call...')
                        most_frequent = list(self.cdb.get_most_frequently_affected())
                        print('successfully called function')
                        output['target'] = most_frequent
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)

        def GET_MOST_DEATHS(self):
                output = { 'result': 'success' }

                try:
                        most_deaths = list(self.cdb.get_most_deaths())
                        output['target'] = most_deaths
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)

        def GET_MOST_AFFECTED(self):
                output = { 'result': 'success' }

                try:
                        most_affected = list(self.cdb.get_most_affected())
                        output['target'] = most_affected
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)

        def GET_BY_NAME(self, cname):
                output = {'result': 'success'}

                try:
                        country = list(self.cdb.get_country_by_name(cname))
                        output['country'] = country
                        total_occurences = 0
                        if(country[1] != "..."):
                                total_occurences = total_occurences + int(country[1])
                        if(country[2] != "..."):
                                total_occurences = total_occurences + int(country[2])
                        if(country[3] != "..."):
                                total_occurences = total_occurences + int(country[3])
                        output['total_occurences'] = total_occurences
                        total_deaths = 0
                        if(country[4] != "..."):
                                total_deaths = total_deaths + int(country[4])
                        if(country[5] != "..."):
                                total_deaths = total_deaths + int(country[5])
                        if(country[6] != "..."):
                                total_deaths = total_deaths + int(country[6])
                        output['total_deaths'] = total_deaths
                        total_affected = 0
                        if(country[7] != "..."):
                                total_affected = total_affected + int(country[7])
                        if(country[8] != "..."):
                                total_affected = total_affected + int(country[8])
                        if(country[9] != "..."):
                                total_affected = total_affected + int(country[9])
                        output['total_affected'] = total_affected
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)
